# Find the best farkle strategy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def rollDice(N):
	return np.random.randint(1,high=7,size=N).tolist()

def calcScoreANDremainingDice(diceSet):
	nums = [1,2,3,4,5,6]
	counts = [diceSet.count(x) for x in nums]
	countCounts = [counts.count(x) for x in nums]
	numDice = len(diceSet)
	
	fivesNones = 0
	triplets = 0
	fourOfAKind = 0
	fiveOfAKind = 0
	sixOfAKind = 0
	straight = 0
	threePairs = 0

	if numDice == 6:
		# Check two triplets
		if countCounts[2] == 2:
			triplets = 2500		
		# Check four with a pair
		if countCounts[3] == 1 and countCounts[1] == 1:
			fourOfAKind = 1500	
		# Check 6 of a kind
		if countCounts[5] == 1:
			sixOfAKind = 3000		
		# Check straight		
		if countCounts[0] == 6:
			straight = 1500		
		# Check three pairs		
		if countCounts[1] == 3:
			threePairs = 1500		
	if numDice >= 5:	
		# Check five of a kind		
		if countCounts[4] == 1:
			fiveOfAKind = 2000		
	if numDice >= 4 and fourOfAKind == 0:	
		# Check four of a kind		
		if countCounts[3] == 1:
			fourOfAKind = 1000
	if numDice >= 3 and triplets == 0:	
		# Check three of a kind
		if countCounts[2] == 1:
			if counts[0] == 3:
				triplets == 300
			else:
				triplets = nums[counts.index(3)] * 100	
	
	## Check fives and ones	
	# If scored from three dice
	if triplets < 2500 and triplets > 0:
		counts[counts.index(3)] -= 3
		if counts[0] >= 3:
			counts[0] -= 3
		if counts[4] >= 3:
			counts[4] -= 3
		fivesNones = counts[0]*100 + counts[4]*50
	# If scored from four dice
	elif fourOfAKind < 1500 and fourOfAKind > 0:
		counts[counts.index(4)] -= 4
		if counts[0] >= 4:
			counts[0] -= 4
		if counts[4] >= 4:
			counts[4] -= 4
		fivesNones = counts[0]*100 + counts[4]*50
	# If scored from five dice
	elif fiveOfAKind == 2000:
		counts[counts.index(5)] -= 5
		if counts[0] >= 5:
			counts[0] -= 5
		if counts[4] >= 5:
			counts[4] -= 5
		fivesNones = counts[0]*100 + counts[4]*50
	# If scored from six dice
	elif triplets == 2500 or fourOfAKind == 1500 or sixOfAKind == 3000 or straight == 1500 or threePairs == 1500:
		counts = [0]*6
		fivesNones = 0
	else:
		fivesNones = counts[0]*100 + counts[4]*50
	counts[0] = 0
	counts[4] = 0
	
	# Return max score with correct number of remaining dice
	maxScore = max(triplets,fourOfAKind,fiveOfAKind,sixOfAKind,straight,threePairs) + fivesNones
	diceLeft = sum(counts)
	return(maxScore,diceLeft)

# Simulate one turn
def farkleTurn(cautionThresh):
	diceLeft = 6
	score = 0
	while diceLeft > cautionThresh:
		roll = rollDice(diceLeft)
		scoreThisRoll,diceLeft = calcScoreANDremainingDice(roll)
		score += scoreThisRoll
		if diceLeft == 0:
			diceLeft = 6
		if scoreThisRoll == 0:
			score = 0
			diceLeft = 0
		if cautionThresh == 0 and score >= 1000:
			cautionThresh = 1
	return score

# Simulate an entire Farkle game (up to 10,000 points)
def playFarkle(cautionThresh):
	scores = []
	while sum(scores) < 10000:
		tmpScore = farkleTurn(cautionThresh)
		scores.append(tmpScore)
	return scores

#--------------------------------------------------------	
# Simulations
#--------------------------------------------------------
cols = ['Coward','Fearful','Careful','Cautious','Brave','Crazy']
rows = range(0,1000)
simDF = pd.DataFrame(np.random.randn(len(rows),len(cols)),index=rows, columns=cols)
for x in rows:
	# Coward = Don't roll if 5 or fewer dice
	# Fearful = Don't roll if 4 or fewer dice
	# Careful = Don't roll if 3 or fewer dice
	# Cautious = Don't roll if 2 or fewer dice
	# Brave = Don't roll 1 or fewer dice
	# Crazy = Always roll until 1500 points, then stop if 1 dice left
	row = np.array([len(playFarkle(5)),len(playFarkle(4)),len(playFarkle(3)),len(playFarkle(2)),len(playFarkle(1)),len(playFarkle(0))])
	simDF.loc[x,:] = row

# Write results to file
print simDF.describe()
writer = pd.ExcelWriter('farkle_simulation_results.xlsx')
simDF.describe().to_excel(writer)
writer.save()

# Plot results as histogram
plt.style.use('ggplot')
bins = np.linspace(0, 140, 100)
for c in cols:
	plt.hist(simDF[c], bins, alpha=0.5, label=c)
plt.ylabel('Count')
plt.xlabel('Number of Turns to Reach 10,000')
plt.legend(loc='best')
plt.savefig('farkle_simulations.jpg')
plt.show()

# Plot results as boxplot
plt.style.use('ggplot')
ax1 = simDF.describe().plot(kind='box')
plt.ylabel('Num. Turns to 10,000')
plt.legend(loc='best')
ax1.set_ylim(0,max(simDF.describe().iloc[7,:]))
plt.savefig('farkle_simulations_boxplt.jpg')
plt.show()