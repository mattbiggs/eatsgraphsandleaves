# Retrieve co-author lists for a list of authors, and find connections

from Bio import Entrez, Medline

Entrez.email = "mb3ad@virginia.edu"     # Always tell NCBI who you are

#------------------------
# Functions
#------------------------
def get_all_pubmed_co_authors(name):
	# Get paper IDs for author from Pubmed
	handle = Entrez.esearch(db="pubmed", term=name, retmax=463)
	record = Entrez.read(handle)
	idlist = record["IdList"]
	print len(idlist)
	
	# Download the corresponding papers from Medline
	handle = Entrez.efetch(db="pubmed", id=idlist, rettype="medline", retmode="text") #,usehistory="y"
	records = Medline.parse(handle)

	# Get author list from each paper
	coauthors = []
	for record in records:
		coauthors += record.get("AU", "?")
	return coauthors

def alternative_names(name,dictionary):
	name_parts = name.split()
	one_initial = name_parts[-1] + ' ' + name_parts[0][0]
	dictionary[name] = name
	dictionary[one_initial] = name
	if len(name_parts) == 3:
		two_initials = name_parts[-1] + ' ' + name_parts[0][0] + name_parts[1][0]
		dictionary[two_initials] = name
	return dictionary

def standardize_names(author_list,dictionary):
	new_author_list = []
	for author in author_list:
		try:
			new_author_list.append(dictionary[author])
		except KeyError:
			new_author_list.append(author)
	return new_author_list

#------------------------
# Main
#------------------------

# Read in all author names
aDictionary = {}
authorList = []
authorFile = open('author_list.txt','r')
for line in authorFile:
	name = line.strip()
	if len(name) > 0:
		authorList.append(name)
		aDictionary = alternative_names(name,aDictionary)
authorFile.close()

# Get all co-authors for each author in the list
coauthor_lists = []
coauthor_lists_file = open('coauthor_lists.tsv','w')
for author in authorList:
	coauthor_list = standardize_names(get_all_pubmed_co_authors(author),aDictionary)
	coauthor_lists.append( coauthor_list )
	coauthor_lists_file.write(author + '\t' + '\t'.join(coauthor_list) + '\n')
coauthor_lists_file.close()

# Count how often authors in the list publish together
output_network = open('coauthorshipNetwork.tsv','w')
shared_authors_list = []
for i in range(len(authorList)-1):
	for j in range(i+1,len(authorList)):
		count = coauthor_lists[i].count(authorList[j])		
		if count > 0:
			output_network.write(authorList[i] + '\t' + str(count) + '\t' + authorList[j] + '\n')
		shared_coauthors = [ca for ca in list(set(coauthor_lists[i])) if (ca in list(set(coauthor_lists[j])) ) and (ca not in authorList)]
		if len(shared_coauthors) > 0:
			sca = 'sca' + str(i) + '_' + str(j)
			shared_authors_list.append(sca)
			output_network.write(authorList[i] + '\t' + str(-1) + '\t' + sca + '\n')				
			output_network.write(authorList[j] + '\t' + str(-1) + '\t' + sca + '\n')
output_network.close()

# Write node types to file (author in main list or shared co-author)
author_attributes = open('authorAttributes.tsv','w')
for author in authorList:
	author_attributes.write(author + '\t' + 'MainAuthor\n')
for author in shared_authors_list:
	author_attributes.write(author + '\t' + 'CommonCoAuthor\n')
author_attributes.close()


