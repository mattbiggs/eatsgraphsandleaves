library(ggplot2)

data = data.frame(y=c(1,5,6,2,4,2,3),x=1:7)

plot(data$x,data$y)

p = ggplot(data, aes(x=x,y=y)) +
    geom_line(alpha=0.5, size=2) +
    geom_point(size=3) + 
    theme_bw() +
    xlab("X Values") +
    ylab("Y Values") +
    theme(text = element_text(size=12))
print(p)

ggsave("simple_plot.jpg",width = 10, height = 6, units = "cm", dpi = 600)
