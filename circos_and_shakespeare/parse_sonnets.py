import random
from difflib import SequenceMatcher as SM
import re

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

#-----------------------------------
# Parse and read in sonnets
#-----------------------------------
# Complete works of William Shakespeare obtained in plain text format from http://www.gutenberg.org/files/100/100.txt
print 'Parsing'
inputfile = open('sonnets.txt','r')

sonnets = []
sonnetIDs = []

curSonnet = ''
curSonnetID = 0

for line in inputfile:
	l = line.strip()
	if is_number(l):
		if curSonnetID != 0:
			sonnets.append(curSonnet.strip())
			sonnetIDs.append(curSonnetID)
		curSonnetID = l
		curSonnet = ''
	else:
		curSonnet += ' ' + l
sonnets.append(curSonnet.strip())
sonnetIDs.append(curSonnetID)
inputfile.close()

#-----------------------------------
# Write karyotype file
#-----------------------------------
print 'Writing karyotype file'
outfile = open('karyotype.sonnets.txt','w')
colors = ['chr12','chr13','chr14','chr15','chr16']
for id in sonnetIDs:
	i = sonnetIDs.index(id)
	outfile.write('chr - ' + 'son' + str(id) + ' ' + str(id) + ' 0 ' + str(len(sonnets[i])) + ' ' + random.choice(colors) + '\n')
outfile.close()

#-----------------------------------
# Find text overlap
#-----------------------------------
print 'Calculating sonnet overlap similarity'
overlapOutputFile = open('sonnet_overlaps.txt','w')
linkNum = 1
for i in range(len(sonnets)-1):
	for j in range(i+1,len(sonnets)):
		son_i = ''.join(sonnets[i].split())
		son_j = ''.join(sonnets[j].split())
		sim = SM(None,son_i,son_j)
		matches = sim.get_matching_blocks()
		for m in matches:
			if m[2] >= 12:
				phrasei = son_i[m[0]:m[0]+m[2]]
				phrasej = son_j[m[1]:m[1]+m[2]]
				print phrasei
				print phrasej
				overlapOutputFile.write('link'  + str(linkNum) + ' son' + str(i) + ' ' + str(son_i.find(phrasei)) + ' ' + str(son_i.find(phrasei) + len(phrasei)) + '\n')
				overlapOutputFile.write('link'  + str(linkNum) + ' son' + str(j) + ' ' + str(son_j.find(phrasej)) + ' ' + str(son_j.find(phrasej) + len(phrasej)) + '\n')
				linkNum += 1
overlapOutputFile.close()

#-----------------------------------
# Calculate frequency of letter "e"
#-----------------------------------
print 'Calculating frequency of letter "e"'
EFrequencyOutputFile = open('sonnet_E_frequency.txt','w')
windowSize = 100
for s in sonnets:
	r = len(s)-windowSize+1
	for i in range(0,r,windowSize-1):
		e = s[i:i+windowSize].count('e')
		e += s[i:i+windowSize].count('E')
		EFrequencyOutputFile.write('son' + str(sonnets.index(s)+1) + ' ' + str(i) + ' ' + str(i + 10) + ' ' + str(float(e)/windowSize) + '\n')
EFrequencyOutputFile.close()

#-----------------------------------
# Calculate frequency of word "love"
#-----------------------------------
print 'Calculating frequency of word "love"'
LOccurrencesOutputFile = open('sonnet_love_occurrences.txt','w')
HOccurrencesOutputFile = open('sonnet_hate_occurrences.txt','w')
for s in sonnets:
	for i in range(0,len(s)-6,10):
		l = s[i:i+7].find('love')
		L = s[i:i+7].find('Love')
		h = s[i:i+7].find('hate')
		H = s[i:i+7].find('hate')
		if l > -1:
			LOccurrencesOutputFile.write('son' + str(sonnets.index(s)+1) + ' ' + str(i+l) + ' ' + str(i+l) + ' 0\n')
		if L > -1:
			LOccurrencesOutputFile.write('son' + str(sonnets.index(s)+1) + ' ' + str(i+L) + ' ' + str(i+L) + ' 0\n')
		if h > -1:
			HOccurrencesOutputFile.write('son' + str(sonnets.index(s)+1) + ' ' + str(i+h) + ' ' + str(i+h) + ' 0\n')
		if H > -1:
			HOccurrencesOutputFile.write('son' + str(sonnets.index(s)+1) + ' ' + str(i+H) + ' ' + str(i+H) + ' 0\n')
LOccurrencesOutputFile.close()
HOccurrencesOutputFile.close()


print 'Done.'






